<?php

use App\Http\Controllers\ClientController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RankingController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

});


Route::get('/products', [ProductController::class, 'products'])->name('products');

Route::get('/products/{product}/delete', [ProductController::class, 'delete']);

Route::get('/products/inserts', function (){
    return view('inserts');
})->name('inserts');


Route::post('/products', [ProductController::class, 'store'])->name('store');

Route::get('/rankings', [RankingController::class, 'showForm'])->name('tops.form');
Route::post('/rankings', [RankingController::class, 'showTop'])->name('tops.show');

Route::get('/clients', [ClientController::class, 'clients'])->name('clients');

Route::get('/clients/{client}/delete', [ClientController::class, 'delete']);

Route::get('/clients/inserts', function (){
    return view('clientinserts');
})->name('clientinserts');

Route::post('/clients', [ClientController::class, 'store'])->name('store');




Route::get('/invoices/list', [InvoiceController::class, 'invoices'])->name('invoices.list');
Route::post('/invoices', [InvoiceController::class, 'store'])->name('invoices.store');

Route::get('/invoices/{invoice}/delete', [InvoiceController::class, 'delete']);
Route::get('/invoices/inserts', [InvoiceController::class, 'passarclients'])->name('invoices.inserts');

Route::post('/invoices/insert/confirm', [InvoiceController::class, 'confirm'])->name('invoices.confirm');

Route::get('/products/{id}/restore', [ProductController::class, 'restore'])->name('products.restore');

require __DIR__.'/auth.php';
