<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class Invoice extends Model
{
    use HasFactory;
    protected $fillable = [
        'client_name',
        'product_name',
        'quantity',
        'price',
        'percentatge_IVA',
        'IVA',
        'total',
    ];
    public $timestamps = false;
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_name', 'client_name');
    }
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_name', 'product_name');
    }
}
