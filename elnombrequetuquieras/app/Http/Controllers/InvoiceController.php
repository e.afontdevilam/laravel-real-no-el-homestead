<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Client;

class InvoiceController extends Controller
{
    public function invoices(){
        $invoice = Invoice::all();
        return view('invoicesview')->with('invoices', $invoice);
    }

    public function delete(Invoice $invoice){
        $invoice->delete();
        return redirect('/invoices/list');
    }

    public function insert(){
        return view('invoicesinserts');
    }


    public function store(Request $request){
        $request->validate([
            'client_name' => 'required|string|max:255',
            'product_name' => 'required|string|max:255',
            'quantity' => 'required|integer',
            'price' => 'required|numeric',
            'percentatge_IVA' => 'required|numeric',
            'IVA' => 'required|numeric',
            'total' => 'required|numeric',
        ]);

        Invoice::create([
            'client_name' => $request->client_name,
            'product_name' => $request->product_name,
            'quantity' => $request->quantity,
            'price' => $request->price,
            'percentatge_IVA' => $request->percentatge_IVA,
            'IVA' => $request->IVA,
            'total' => $request->total,
        ]);

        return redirect('/invoices/list')->with('success', 'Comanda inserida correctament.');
    }


    public function passarclients()
    {

        $clients = Client::all('client_name');
        $products = Product::all('product_name');
        return view('invoiceinserts', compact('clients', 'products'));
    }


    public function confirm(Request $request) {
        $validated = $request->validate([
            'client_name' => 'required|string|max:255',
            'product_name' => 'required|string|max:255',
            'quantity' => 'required|integer|min:1',
            'password' => 'required|string',
        ]);

        $client = Client::where('client_name', $validated['client_name'])->first();
        $product = Product::where('product_name', $validated['product_name'])->first();

        $price = $product->price;

        if ($product->product_type === 'Weapon' || $product->product_type === 'Armour' || $product->product_type === 'Shield') {
            $ivaPercentage = 21;
        } elseif ($product->product_type === 'Potion' || $product->product_type === 'Consumable') {
            $ivaPercentage = 4;
        } elseif ($product->product_type === 'Projectile') {
            $ivaPercentage = 10;
        } else {
            $ivaPercentage = 0;
        }

        $quantity = $validated['quantity'];
        $iva = ($price * $ivaPercentage / 100) * $quantity;
        $total = ($price * $quantity) + $iva;

        $clientmoney = $client->current_money;

        $clientlevel = $client->level;
        $productlevelrequired = $product->level_required;
        $stock = $product->stock_left;

        $clientpassword = $client->password;

        if ($clientmoney >= $total && $clientlevel >= $productlevelrequired && $stock >= $quantity && $clientpassword === $validated['password']) {
            $client->increment('purchases');
            $product->decrement('stock_left', $quantity);
            $product->increment('units_sold', $quantity);
            $product->increment('revenue', $total);
            $client->decrement('current_money', $total);
            Invoice::create([
                'client_name' => $client->client_name,
                'product_name' => $product->product_name,
                'quantity' => $quantity,
                'price' => $price,
                'percentatge_IVA' => $ivaPercentage,
                'IVA' => $iva,
                'total' => $total,
                'created_at'=>now(),
                'updated_at'=>now(),
            ]);


            return redirect('/invoices/list')->with('success', 'Comanda inserida correctament.');
        }else if($clientmoney < $total && $clientlevel >= $productlevelrequired) {
            return redirect()->back()->with('error', 'El client no té suficient diners per comprar el producte. Explora alguna masmorra per a aconseguir-ne més!');
        }else if ($clientlevel<$productlevelrequired && $clientmoney >= $total){
            return redirect()->back()->with('error', 'El client no té el nivell requerit per comprar el producte. Explora alguna masmorra per a pujar de nivell!');
        }else if($clientmoney < $total && $clientlevel<$productlevelrequired){
            return redirect()->back()->with('error', 'El client no té el nivell requerit per comprar el producte ni els diners suficients. Explora alguna masmorra per a aconseguir més diners i pujar de nivell!');
        }else if($stock == 0 || $stock-$quantity<=0){
            return redirect()->back()->with('error', 'No hi ha existències suficients!');
        }else if($clientpassword != $validated['password']){
            return redirect()->back()->with('error', 'Contrasenya incorrecta. Estafador!');
        }


    }
}
