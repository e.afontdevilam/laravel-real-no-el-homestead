<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Stock;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function products(){
        $product = Product::all();
        return view('productsview')->with('products', $product);
    }

    public function delete(Product $product){
        $product->delete();
        return redirect('/products');
    }

    public function insert(){
        return view('inserts');
    }

    public function topSoldProducts()
    {
        $topProducts = Product::orderBy('units_sold', 'desc')->take(5)->get();
        return view('rankings', ['topProducts' => $topProducts]);
    }


    public function store(Request $request){
        $request->validate([
            'product_name' => 'required|string|max:255',
            'product_type' => 'required|string|in:Weapon,Armour,Shield,Potion,Consumable,Projectile',
            'price' => 'required|numeric',
            'description' => 'required|string',
            'level' => 'required|integer',
            'level_required' => 'required|integer',
        ]);

        Product::insert([
            'product_name' => $request->product_name,
            'product_type' => $request->product_type,
            'price' => $request->price,
            'description' => $request->description,
            'level' => $request->level,
            'level_required' => $request->level_required,

        ]);

        return redirect('/products')->with('success', 'Producto insertado correctamente.');
    }
    public function restore($id)
    {
        $product = Product::find($id);
        if ($product) {
            $product->increment('stock_left', 100);
            return redirect()->route('products')->with('success', 'Stock restored successfully.');
        } else {
            return redirect()->route('products')->with('error', 'Product not found.');
        }
    }

}
