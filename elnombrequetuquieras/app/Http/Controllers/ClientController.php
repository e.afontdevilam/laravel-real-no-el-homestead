<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function clients(){
        $client = Client::all();
        return view('clientsview')->with('clients', $client);
    }

    public function delete(Client $client){
        $client->delete();
        return redirect('/clients');
    }

    public function insert(){
        return view('clientinserts');
    }


    public function store(Request $request){
        $request->validate([
            'client_name' => 'required|string|max:255',
            'race' => 'required|string|max:255',
            'level' => 'required|integer',
            'current_money' => 'required|numeric',
            'description' => 'required|string',
            'password' => 'required|string',
        ]);

        Client::insert([
            'client_name' => $request->client_name,
            'race' => $request->race,
            'level' => $request->level,
            'current_money' => $request->current_money,
            'description' => $request->description,
            'password' => $request->password,
        ]);

        return redirect('/clients')->with('success', 'Client inserit correctament.');
    }
}
