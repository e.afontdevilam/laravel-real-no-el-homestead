<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class RankingController extends Controller
{
    public function showForm()
    {
        return view('rankings');
    }

    public function showTop(Request $request)
    {
        $topType = $request->top_type;
        $topProducts = [];


        if ($topType === 'top_sold') {

            $topProducts = Product::orderBy('units_sold', 'desc')->take(5)->get();
        } elseif ($topType === 'top_revenue') {

            $topProducts = Product::orderBy('revenue', 'desc')->take(5)->get();
        } elseif ($topType === 'most_stock') {

            $topProducts = Product::orderBy('stock_left', 'desc')->take(5)->get();
        } elseif ($topType === 'expensive') {

            $topProducts = Product::orderBy('price', 'desc')->take(5)->get();
        } elseif ($topType === 'cheap') {

            $topProducts = Product::orderBy('price', 'asc')->take(5)->get();
        }

        return view('tops', compact('topType', 'topProducts'));
    }
}
