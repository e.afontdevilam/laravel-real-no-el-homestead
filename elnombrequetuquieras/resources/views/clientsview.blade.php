<div>
    <!-- The best way to take care of the future is to take care of the present moment. - Thich Nhat Hanh -->
</div>

<x-app-layout>
    <div>

        <a href="{{ route('products') }}" class="btn btn-primary" style=" background-color: #1e40af; color: white">Productos</a>
        <a href="{{ route('invoices.inserts') }}" class="btn btn-primary" style=" background-color: #1e40af; color: white">Comprar</a>
        <h1 style="font-size: 3rem; color: #1e40af; font-weight: bold;text-align: center;margin-top: 1rem;">CLIENTS</h1>
    </div>
    <div style="display: flex;
            justify-content: center;">
    <table>
        <thead>
        <tr>
            <th>id</th>
            <th>name</th>
            <th>race</th>
            <th>level</th>
            <th>current_money</th>
            <th>description</th>
            <th>purchases</th>
        </tr>
        </thead>

        <body>
        @foreach($clients as $cli)
            <tr>
                <td>{{$cli->id}}</td>
                <td>{{$cli->client_name}}</td>
                <td>{{$cli->race}}</td>
                <td>{{$cli->level}}</td>
                <td>{{$cli->current_money}}</td>
                <td>{{$cli->description}}</td>
                <td>{{$cli->purchases}}</td>
                <td><button type="submit" onclick="location.href='{{url(('/clients/'.$cli->id.'/delete/'))}}'">Delete</button></td>
            </tr>



        @endforeach



        </body>
    </table>
    </div>
    <div style="text-align: center;">
        <a href="{{ route('clientinserts') }}" class="btn btn-primary" style=" background-color: #1e40af; color: white; padding: 1rem; border-radius: 0.25rem; display: inline-block">Insertar Cliente</a>

    </div>
</x-app-layout>
