<div>
    <!-- Smile, breathe, and go slowly. - Thich Nhat Hanh -->
</div>

<!-- resources/views/inserts.blade.php -->
<x-app-layout>
    <div>
        <a href="{{ route('products') }}" class="btn btn-primary" style=" background-color: #1e40af; color: white">Productes</a>

        <h1 style="font-size: 2rem; color: #1e40af; font-weight: bold;text-align: center;margin-top: 1rem;">Emplena el formulari amb els detalls del nou producte</h1>
    </div>
    <div style="display: flex;
            justify-content: center;">
    <form method="POST" action="{{ route('products') }}">
        @csrf
        <div>
            <label for="product_name">Nombre del Producto:</label>
            <input type="text" id="product_name" name="product_name" required>
        </div>
        <br>
        <div>
            <label for="product_type">Tipo de Producto:</label>

            <select id="product_type" name="product_type">
                <option value="Weapon">Weapon</option>
                <option value="Armour">Armour</option>
                <option value="Shield">Shield</option>
                <option value="Potion">Potion</option>
                <option value="Projectile">Projectile</option>
                <option value="Consumable">Consumable</option>
            </select>
        </div>
        <br>
        <div>
            <label for="price">Precio:</label>
            <input type="number" id="price" name="price" required>
        </div><br>
        <div>
            <label for="description">Descripción:</label>
            <textarea id="description" name="description" required></textarea>
        </div><br>
        <div>
            <label for="level">Nivel:</label>
            <input type="number" id="level" name="level" required>
        </div><br>
        <div>
            <label for="level_required">Nivel Requerido:</label>
            <input type="number" id="level_required" name="level_required" required>
        </div><br>


        <div>
            <button type="submit">Insertar Producto</button>
        </div>
    </form>
    </div>
    <div>



    </div>
</x-app-layout>





