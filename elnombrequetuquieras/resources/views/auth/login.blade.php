<x-guest-layout>
    <!-- Session Status -->
    <x-auth-session-status class="mb-4" :status="session('status')" />

    <form method="POST" action="{{ route('login') }}">
        @csrf

        <!-- Aquí van tus campos de inicio de sesión -->

        <button type="submit" class="btn btn-primary">Iniciar sesión</button>

        <!-- Añadimos un enlace para un "inicio de sesión rápido" -->
        <a href="{{ route('dev.login') }}">Inicio de sesión rápido como usuario específico</a>
    </form>

</x-guest-layout>
