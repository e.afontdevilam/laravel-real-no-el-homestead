<div>
    <!-- Live as if you were to die tomorrow. Learn as if you were to live forever. - Mahatma Gandhi -->
</div>


<x-app-layout>
    <div>

        <a href="{{ route('products') }}" class="btn btn-primary" style=" background-color: #1e40af; color: white">Productes</a>
        <h1 style="font-size: 3rem; color: #1e40af; font-weight: bold;text-align: center;margin-top: 1rem;">INVOICES</h1>

    </div>
<div style="display: flex;
            justify-content: center;">
    <table>
        <thead>
        <tr>
            <th>id</th>
            <th>client_name</th>
            <th>product</th>
            <th>quantity</th>
            <th>price</th>
            <th>IVA_percentage</th>
            <th>IVA</th>
            <th>total</th>
        </tr>
        </thead>

        <body>
        @foreach($invoices as $inv)
            <tr>
                <td>{{$inv->id}}</td>
                <td>{{$inv->client_name}}</td>
                <td>{{$inv->product_name}}</td>
                <td>{{$inv->quantity}}</td>
                <td>{{$inv->price}}</td>
                <td>{{$inv->percentatge_IVA}}</td>
                <td>{{$inv->IVA}}</td>
                <td>{{$inv->total}}</td>
                <td><button type="submit" onclick="location.href='{{url(('/invoices/'.$inv->id.'/delete/'))}}'">Delete</button></td>
            </tr>



        @endforeach



        </body>
    </table>
</div>
    <a href="{{ route('invoices.inserts') }}" class="btn btn-primary" style=" background-color: #1e40af; color: white; padding: 1rem; border-radius: 0.25rem; display: inline-block">Comprar</a>
    <div>

    </div>
</x-app-layout>
