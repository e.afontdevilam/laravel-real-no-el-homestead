<div>
    <!-- Nothing worth having comes easy. - Theodore Roosevelt -->
    <!-- It is not the man who has too little, but the man who craves more, that is poor. - Seneca -->
</div>

<x-app-layout>
    <div>

        <a href="{{ route('clients') }}" class="btn btn-primary" style=" background-color: #1e40af; color: white">Clients</a>
        <a href="{{ route('invoices.inserts') }}" class="btn btn-primary" style=" background-color: #1e40af; color: white">Comprar</a>
        <h1 style="font-size: 3rem; color: #1e40af; font-weight: bold;text-align: center;margin-top: 1rem;">PRODUCTS</h1>
    </div>
    <div style="display: flex;
            justify-content: center;">
<table>
    <thead>
   <tr>
       <th>id</th>
       <th>name</th>
       <th>type</th>
       <th>price</th>
       <th>description</th>
       <th>level</th>
       <th>level required</th>
       <th>stock left</th>
       <th>units sold</th>
       <th>revenue</th>
   </tr>
    </thead>

    <body>
    @foreach($products as $pro)
        <tr>
            <td>{{$pro->id}}</td>
            <td>{{$pro->product_name}}</td>
            <td>{{$pro->product_type}}</td>
            <td>{{$pro->price}}</td>
            <td>{{$pro->description}}</td>
            <td>{{$pro->level}}</td>
            <td>{{$pro->level_required}}</td>
            <td>{{$pro->stock_left}}</td>
            <td>{{$pro->units_sold}}</td>
            <td>{{$pro->revenue}}</td>
            <td><button type="submit" onclick="location.href='{{url(('/products/'.$pro->id.'/delete/'))}}'">Delete</button></td>
            <td><button type="submit" onclick="location.href='{{ route('products.restore', $pro->id) }}'">Restore</button></td>
        </tr>



    @endforeach



    </body>
</table>
        </div>

    <div style="text-align: center;">
        <a href="{{ route('inserts') }}" class="btn btn-primary" style=" background-color: #1e40af; color: white; padding: 1rem; border-radius: 0.25rem; display: inline-block">Insertar Producto</a>
        <a href="{{ route('tops.form') }}" class="btn btn-primary" style=" background-color: #1e40af; color: white;  padding: 1rem; border-radius: 0.25rem; display: inline-block">Rànkings</a>

</div>
</x-app-layout>
