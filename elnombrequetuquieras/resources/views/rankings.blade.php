<div>
    <!-- Nothing worth having comes easy. - Theodore Roosevelt -->
</div>


<x-app-layout>
    <a href="{{ route('products') }}" class="btn btn-primary" style=" background-color: #1e40af; color: white">Productes</a>
    <a href="{{ route('clients') }}" class="btn btn-primary" style=" background-color: #1e40af; color: white">Clients</a>
    <a href="{{ route('invoices.inserts') }}" class="btn btn-primary" style=" background-color: #1e40af; color: white">Comprar</a>

    <h1 style="font-size: 3rem; color: #1e40af; font-weight: bold;text-align: center;margin-top: 1rem;">RÀNKINGS</h1>

    <div style="display: flex;
            justify-content: center;">
    <form method="POST" action="{{ route('tops.show') }}">
        @csrf
        <label for="top_type">Selecciona el tipo de top:</label>
        <select id="top_type" name="top_type">
            <option value="top_sold">Top 5 productes més venuts</option>
            <option value="top_revenue">Top 5 productes amb major revenue</option>
            <option value="most_stock">Top 5 productes amb més stock disponible</option>
            <option value="expensive">Top 5 productes amb més cars</option>
            <option value="cheap">Top 5 productes amb més barats</option>
        </select>
        <button type="submit">Consultar Top</button>
    </form>
    </div>


</x-app-layout>
