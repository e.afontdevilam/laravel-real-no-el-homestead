<div>
    <!-- Simplicity is the essence of happiness. - Cedric Bledsoe -->
</div>
<x-app-layout>
    <div>
        <a href="{{ route('clients') }}" class="btn btn-primary" style=" background-color: #1e40af; color: white">Clients</a>
        <a href="{{ route('products') }}" class="btn btn-primary" style=" background-color: #1e40af; color: white">Productes</a>
        <a href="{{ route('tops.form') }}" class="btn btn-primary" style=" background-color: #1e40af; color: white">Rànkings</a>

        <h1 style="font-size: 2rem; color: #1e40af; font-weight: bold;text-align: center;margin-top: 1rem;">Emplena el formulari amb els teus detalls</h1>

    </div>

    <div style="display: flex;
            justify-content: center;">
    <form method="POST" action="{{ route('clients') }}">
        @csrf
        <div>
            <label for="client_name">Nombre del Cliente:</label>
            <input type="text" id="client_name" name="client_name" required>
        </div><br>
        <div>
            <label for="race">Raza:</label>
            <select id="race" name="race" required>
                <option value="human">Human</option>
                <option value="saiyan">Saiyan</option>
                <option value="giant">Giant</option>
                <option value="fairy">Fairy</option>
                <option value="monster">Monster</option>
                <option value="elf">Elf</option>
                <option value="dwarf">Dwarf</option>
                <option value="werewolf">Werewolf</option>
                <option value="mermaid">Mermaid</option>
                <option value="gnome">Gnome</option>
                <option value="robot">Robot</option>
                <option value="centaur">Centaur</option>
            </select>
        </div><br>
        <div>
            <label for="level">Nivel:</label>
            <input type="number" id="level" name="level" required>
        </div><br>

        <div>
            <label for="current_money">Dinero actual:</label>
            <input type="number" id="current_money" name="current_money" required>
        </div><br>
        <div>
            <label for="description">Descripción:</label>
            <textarea id="description" name="description" required></textarea>
        </div><br>
        <div>
            <label for="password">Password:</label>
            <textarea id="password" name="password" required></textarea>
        </div><br>
        <div>
            <button type="submit">Insertar Cliente</button>
        </div><br>
    </form>
    </div>

    <div style="text-align: center;">
        <h1 style="font-size: 1rem; color: #1e40af; font-weight: bold;text-align: center;margin-top: 1rem;">Ja ets client? Ves a la pantalla de compra des d'aquí mateix!</h1>
        <a href="{{ route('invoices.inserts') }}" class="btn btn-primary" style=" background-color: #1e40af; color: white;  padding: 1rem; border-radius: 0.25rem; display: inline-block">Comprar</a>

    </div>
</x-app-layout>
