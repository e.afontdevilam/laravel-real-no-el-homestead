<div>
    <!-- Nothing in life is to be feared, it is only to be understood. Now is the time to understand more, so that we may fear less. - Marie Curie -->
</div>

<x-app-layout>
    <div>
        <h2>Confirmació de factura</h2>
        <p>Nom del client: {{ $client_name }}</p>
        <p>Producte: {{ $product_name }}</p>
        <p>Quantitat: {{ $quantity }}</p>
        <p>Preu unitari: {{ number_format($price, 2) }}</p>
        <p>Percentatge d'IVA: {{ number_format($ivaPercentage, 2) }}%</p>
        <p>IVA: {{ number_format($iva, 2) }}</p>
        <p>Total: {{ number_format($total, 2) }}</p>

        <form method="POST" action="{{ route('invoices.store') }}">
            @csrf
            <input type="hidden" name="client_name" value="{{ $client_name }}">
            <input type="hidden" name="product_name" value="{{ $product_name }}">
            <input type="hidden" name="quantity" value="{{ $quantity }}">
            <input type="hidden" name="price" value="{{ $price }}">
            <input type="hidden" name="ivaPercentage" value="{{ $ivaPercentage }}">
            <input type="hidden" name="iva" value="{{ $iva }}">
            <input type="hidden" name="total" value="{{ $total }}">
            <button type="submit">Confirmar i realitzar compra</button>
        </form>
    </div>
</x-app-layout>
