<div>
    <!-- Simplicity is an acquired taste. - Katharine Gerould -->
</div>



<x-app-layout>

    <div>
        <a href="{{ route('products') }}" class="btn btn-primary" style=" background-color: #1e40af; color: white">Productes</a>
        <a href="{{ route('invoices.list') }}" class="btn btn-primary" style=" background-color: #1e40af; color: white">Comandes</a>

        <h1 style="font-size: 2rem; color: #1e40af; font-weight: bold;text-align: center;margin-top: 1rem;">Emplena el formulari amb els detalls de la teva compra</h1>
    </div>

    <div style="display: flex;
            justify-content: center;">
    <form method="POST" action="{{ route('invoices.confirm') }}">
        @csrf
        <div>
            <label for="client_search">Buscar Cliente:</label>
            <input type="text" id="client_search" placeholder="Buscar Cliente" class="form-control">
            <select id="client_name" name="client_name" class="form-control" required>
                @foreach($clients as $cli)
                    <option value="{{ $cli->client_name }}">{{ $cli->client_name }}</option>
                @endforeach
            </select><br><br>
        </div>
        <div>
            <label for="product_search">Buscar Producto:</label>
            <input type="text" id="product_search" placeholder="Buscar Producto" class="form-control">
            <select id="product_name" name="product_name" class="form-control" required>
                @foreach($products as $pro)
                    <option value="{{ $pro->product_name }}">{{ $pro->product_name }}</option>
                @endforeach
            </select><br><br>
        </div>
        <div>
            <label for="quantity">Quantitat:</label>
            <input type="number" id="quantity" name="quantity" required>
        </div><br>

        <div>
            <label for="password">Password:</label>
            <input type="text" id="password" name="password" required>
        </div><br>

        <div>
            <button type="submit" class="btn btn-primary">Realitzar compra</button>
        </div>
    </form>
    </div>
    <div style="text-align: center;">
        <h1 style="font-size: 1rem; color: #1e40af; font-weight: bold;text-align: center;margin-top: 1rem;">Encara no ets client? Enregistra't a la nostra base de dades aquí mateix!</h1>

        <a href="{{ route('clientinserts') }}" class="btn btn-primary" style=" background-color: #1e40af; color: white; padding: 1rem; border-radius: 0.25rem; display: inline-block">Enregistrar-se</a>

    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function () {
            const clientSearchInput = document.getElementById('client_search');
            const clientSelect = document.getElementById('client_name');
            const productSearchInput = document.getElementById('product_search');
            const productSelect = document.getElementById('product_name');

            clientSearchInput.addEventListener('keyup', function () {
                filterOptions(clientSearchInput.value, clientSelect);
            });

            productSearchInput.addEventListener('keyup', function () {
                filterOptions(productSearchInput.value, productSelect);
            });

            function filterOptions(searchTerm, selectElement) {
                const options = selectElement.querySelectorAll('option');
                options.forEach(option => {
                    if (option.text.toLowerCase().includes(searchTerm.toLowerCase())) {
                        option.style.display = '';
                    } else {
                        option.style.display = 'none';
                    }
                });
            }
        });
    </script>
</x-app-layout>

