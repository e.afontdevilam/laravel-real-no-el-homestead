<div>
    <!-- Do what you can, with what you have, where you are. - Theodore Roosevelt -->
</div>


<x-app-layout>
    <a href="{{ route('products') }}" class="btn btn-primary" style=" background-color: #1e40af; color: white">Productes</a>
    <a href="{{ route('clients') }}" class="btn btn-primary" style=" background-color: #1e40af; color: white">Clients</a>
    <a href="{{ route('invoices.inserts') }}" class="btn btn-primary" style=" background-color: #1e40af; color: white">Comprar</a>

    <h1 style="font-size: 3rem; color: #1e40af; font-weight: bold;text-align: center;margin-top: 1rem;">RÀNKINGS</h1>

    <div style="display: flex;
            justify-content: center;">
    <form method="POST" action="{{ route('tops.show') }}">
        @csrf
        <label for="top_type">Selecciona el tipo de top:</label>
        <select id="top_type" name="top_type">
            <option value="top_sold">Top 5 productes més venuts</option>
            <option value="top_revenue">Top 5 productes amb major revenue</option>
            <option value="most_stock">Top 5 productes amb més stock disponible</option>
            <option value="expensive">Top 5 productes més cars</option>
            <option value="cheap">Top 5 productes més barats</option>
        </select>
        <button type="submit">Consultar Top</button>
    </form>
    </div>
    <div style="display: flex;flex-direction: column;align-items: center;
            justify-content: center; ">
    @foreach ($topProducts as $product)
        @if ($topType === 'top_sold')
            <p>{{ $product->product_name }} - {{ $product->units_sold }} unitats venudes</p>
        @elseif ($topType === 'top_revenue')
            <p>{{ $product->product_name }} - {{ $product->revenue }} en revenue</p>
        @elseif ($topType === 'most_stock')
            <p>{{ $product->product_name }} - {{ $product->stock_left }} en stock disponible</p>
        @elseif ($topType === 'expensive')
            <p>{{ $product->product_name }} - Preu: {{ $product->price }}</p>
        @elseif ($topType === 'cheap')
            <p>{{ $product->product_name }} - Preu: {{ $product->price }}</p>
        @endif

    @endforeach
    </div>


</x-app-layout>
