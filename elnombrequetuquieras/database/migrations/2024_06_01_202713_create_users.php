<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string('client_name')->unique();
            $table->enum('race',['Human', 'Saiyan', 'Giant', 'Fairy', 'Monster', 'Elf', 'Dwarf', 'Werewolf', 'Mermaid', 'Gnome', 'Robot', 'Centaur']);
            $table->integer('level');
            $table->double('current_money');
            $table->string('description')->unique();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {

    }
};
