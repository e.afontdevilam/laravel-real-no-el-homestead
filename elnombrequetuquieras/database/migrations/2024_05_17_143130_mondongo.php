<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('productsshop', function (Blueprint $table) {
            $table->id();
            $table->string('product_name')->unique();
            $table->enum('product_type',['Weapon', 'Armour', 'Shield', 'Potion', 'Projectile', 'Consumable']);
            $table->double('price');
            $table->string('description')->unique();
            $table->integer('level');
            $table->integer('level_required');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
