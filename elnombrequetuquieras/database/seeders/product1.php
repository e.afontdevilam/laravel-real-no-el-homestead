<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class product1 extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('stocks')->insert([
            'product_name' => 'Buster Sword',
            'product_type' => 'Weapon',
            'price' => '10000',
            'description' => 'An iconic videogame weapon that belongs to Cloud Strife, the main character from FINAL FANTASY VII',
            'level' => '7',
            'level_required' => '30'
        ]);
    }
}
