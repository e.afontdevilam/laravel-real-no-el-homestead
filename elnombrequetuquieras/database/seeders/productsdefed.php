<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class productsdefed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('products')->insert([
            ['product_name' => 'Keyblade',
                'product_type' => 'Weapon',
                'price' => '15000',
                'description' => 'An iconic videogame weapon that belongs to Sora, the main character from KINGDOM HEARTS',
                'level' => '3',
                'level_required' => '40'],

            ['product_name' => 'Gula',
                'product_type' => 'Potion',
                'price' => '7000',
                'description' => 'A potion that fills your hunger gauge',
                'level' => '5',
                'level_required' => '20'],

            ['product_name' => 'Hamburguer',
                'product_type' => 'Consumable',
                'price' => '1000',
                'description' => 'An hamburguer that, when consumed, restores a bit your hunger gauge',
                'level' => '1',
                'level_required' => '5'],

            ['product_name' => 'Hardisk',
                'product_type' => 'Projectile',
                'price' => '9000',
                'description' => 'A really hard disk that hits hard when thrown at an opponent. It also contains images and other files that are irrelevant',
                'level' => '5',
                'level_required' => '100'],

            [
                'product_name' => 'Buster Sword',
                'product_type' => 'Weapon',
                'price' => '10000',
                'description' => 'An iconic videogame weapon that belongs to Cloud Strife, the main character from FINAL FANTASY VII',
                'level' => '7',
                'level_required' => '30'
            ],
            ['product_name' => 'Poké Ball',
                'product_type' => 'Projectile',
                'price' => '1000',
                'description' => 'An iconic videogame object that is used by POKÉMON TRAINERS to catch Pokémon',
                'level' => '1',
                'level_required' => '20'],

            ['product_name' => 'Muscle Potion',
                'product_type' => 'Potion',
                'price' => '9000',
                'description' => 'A potion that boosts your attack stat temporarily',
                'level' => '5',
                'level_required' => '25'],

            ['product_name' => 'Waffle',
                'product_type' => 'Consumable',
                'price' => '1500',
                'description' => 'A waffle that, when consumed, restores a bit your hunger gauge',
                'level' => '1',
                'level_required' => '10'],

            ['product_name' => 'Lightsaber',
                'product_type' => 'Weapon',
                'price' => '15000',
                'description' => 'An iconic film weapon that belongs to a Jedi, important characters from STAR WARS. Its color depends on your honor value.',
                'level' => '20',
                'level_required' => '180'],

            ['product_name' => 'esborrableA',
                'product_type' => 'Projectile',
                'price' => '0',
                'description' => 'esborrableA',
                'level' => '0',
                'level_required' => '0'],

            ['product_name' => 'esborrableB',
                'product_type' => 'Projectile',
                'price' => '0',
                'description' => 'esborrableB',
                'level' => '0',
                'level_required' => '0'],

            ['product_name' => 'esborrableC',
                'product_type' => 'Projectile',
                'price' => '0',
                'description' => 'esborrableC',
                'level' => '0',
                'level_required' => '0'],

            ['product_name' => 'esborrableD',
                'product_type' => 'Projectile',
                'price' => '0',
                'description' => 'esborrableD',
                'level' => '0',
                'level_required' => '0'],

            ['product_name' => 'esborrableE',
                'product_type' => 'Projectile',
                'price' => '0',
                'description' => 'esborrableE',
                'level' => '0',
                'level_required' => '0'],

            ['product_name' => 'esborrableF',
                'product_type' => 'Projectile',
                'price' => '0',
                'description' => 'esborrableF',
                'level' => '0',
                'level_required' => '0'],

            ['product_name' => 'esborrableG',
                'product_type' => 'Projectile',
                'price' => '0',
                'description' => 'esborrableG',
                'level' => '0',
                'level_required' => '0']





        ]);

    }
}
