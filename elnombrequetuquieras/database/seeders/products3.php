<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class products3 extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('stocks')->insert([
            ['product_name' => 'Poké Ball',
                'product_type' => 'Projectile',
                'price' => '1000',
                'description' => 'An iconic videogame object that is used by POKÉMON TRAINERS to catch Pokémon',
                'level' => '1',
                'level_required' => '20'],

            ['product_name' => 'Muscle Potion',
                'product_type' => 'Potion',
                'price' => '9000',
                'description' => 'A potion that boosts your attack stat temporarily',
                'level' => '5',
                'level_required' => '25'],

            ['product_name' => 'Waffle',
                'product_type' => 'Consumable',
                'price' => '1500',
                'description' => 'A waffle that, when consumed, restores a bit your hunger gauge',
                'level' => '1',
                'level_required' => '10'],

            ['product_name' => 'Lightsaber',
                'product_type' => 'Weapon',
                'price' => '15000',
                'description' => 'An iconic film weapon that belongs to a Jedi, important characters from STAR WARS. Its color depends on your honor value.',
                'level' => '20',
                'level_required' => '180']

        ]);

    }
}
