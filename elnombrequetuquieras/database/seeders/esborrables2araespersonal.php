<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class esborrables2araespersonal extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('stocks')->insert([
            ['product_name' => 'esborrableA',
                'product_type' => 'Projectile',
                'price' => '0',
                'description' => 'esborrableA',
                'level' => '0',
                'level_required' => '0'],

            ['product_name' => 'esborrableB',
                'product_type' => 'Projectile',
                'price' => '0',
                'description' => 'esborrableB',
                'level' => '0',
                'level_required' => '0'],

            ['product_name' => 'esborrableC',
                'product_type' => 'Projectile',
                'price' => '0',
                'description' => 'esborrableC',
                'level' => '0',
                'level_required' => '0'],

            ['product_name' => 'esborrableD',
                'product_type' => 'Projectile',
                'price' => '0',
                'description' => 'esborrableD',
                'level' => '0',
                'level_required' => '0'],

            ['product_name' => 'esborrableE',
                'product_type' => 'Projectile',
                'price' => '0',
                'description' => 'esborrableE',
                'level' => '0',
                'level_required' => '0'],

            ['product_name' => 'esborrableF',
                'product_type' => 'Projectile',
                'price' => '0',
                'description' => 'esborrableF',
                'level' => '0',
                'level_required' => '0'],

            ['product_name' => 'esborrableG',
                'product_type' => 'Projectile',
                'price' => '0',
                'description' => 'esborrableG',
                'level' => '0',
                'level_required' => '0'],

        ]);

    }
}
