<?php

namespace Database\Seeders;

use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            ClientsInicialsSeeder::class,
            EsborrablesSeeder::class,
            Esborrables2AraesPersonalSeeder::class,
            Product1Seeder::class,
            ProductsSeeder::class,
            Products2Seeder::class,
            Products3Seeder::class,
            ProductsDefedSeeder::class,
            SeederShopSeeder::class,
        ]);
    }
}
