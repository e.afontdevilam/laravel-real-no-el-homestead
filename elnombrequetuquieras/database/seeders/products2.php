<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class products2 extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('stocks')->insert([
            ['product_name' => 'Keyblade',
            'product_type' => 'Weapon',
            'price' => '15000',
            'description' => 'An iconic videogame weapon that belongs to Sora, the main character from KINGDOM HEARTS',
            'level' => '3',
            'level_required' => '40'],

            ['product_name' => 'Gula',
                'product_type' => 'Potion',
                'price' => '7000',
                'description' => 'A potion that fills your hunger gauge',
                'level' => '5',
                'level_required' => '20'],

            ['product_name' => 'Hamburguer',
                'product_type' => 'Consumable',
                'price' => '1000',
                'description' => 'An hamburguer that, when consumed, restores a bit your hunger gauge',
                'level' => '1',
                'level_required' => '5'],

            ['product_name' => 'Hardisk',
                'product_type' => 'Projectile',
                'price' => '9000',
                'description' => 'A really hard disk that hits hard when thrown at an opponent. It also contains images and other files that are irrelevant',
                'level' => '5',
                'level_required' => '100']

        ]);

    }
}
