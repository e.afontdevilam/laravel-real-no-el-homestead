<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class clients_inicials extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('clients')->insert([
            ['client_name' => 'Arjanosky',
                'race' => 'Human',
                'level' => '234',
                'current_money' => '100000',
                'description' => 'A pretty nice guy who always does his best despite not always achieving what he wants, but never gives up and that is what makes him invincible'
                ],

            ['client_name' => 'Goku',
                'race' => 'Saiyan',
                'level' => '74',
                'current_money' => '9000',
                'description' => 'Hey, it is me, Goku! I have heard that you are pretty strong, huh?'
            ],

            ['client_name' => 'Leafa',
                'race' => 'Fairy',
                'level' => '2',
                'current_money' => '100',
                'description' => 'I just started playing, do not expect me to get any cool stuff anytime soon'
            ],



        ]);
}}
