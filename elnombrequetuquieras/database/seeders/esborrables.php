<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class esborrables extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('stocks')->insert([
            ['product_name' => 'esborrable',
                'product_type' => 'Projectile',
                'price' => '0',
                'description' => 'esborrable',
                'level' => '0',
                'level_required' => '0'],

            ['product_name' => 'esborrable2',
                'product_type' => 'Projectile',
                'price' => '0',
                'description' => 'esborrable2',
                'level' => '0',
                'level_required' => '0'],

            ['product_name' => 'esborrable3',
                'product_type' => 'Projectile',
                'price' => '0',
                'description' => 'esborrable3',
                'level' => '0',
                'level_required' => '0'],

            ['product_name' => 'esborrable4',
                'product_type' => 'Projectile',
                'price' => '0',
                'description' => 'esborrable4',
                'level' => '0',
                'level_required' => '0']

        ]);

    }
}
